package io.supgo.QRReader

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.graphics.PointF
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback
import com.dlazaro66.qrcodereaderview.QRCodeReaderView
import com.dlazaro66.qrcodereaderview.QRCodeReaderView.OnQRCodeReadListener
import com.google.android.material.snackbar.Snackbar
import io.supgo.keycloaker.R
import io.supgo.keycloaker.storage.IOAuth2AccessTokenStorage
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.android.inject


class DecoderActivity : AppCompatActivity(), OnRequestPermissionsResultCallback, OnQRCodeReadListener {

    private var mainLayout: ViewGroup? = null
    private var qrCodeReaderView: QRCodeReaderView? = null
    private var pointsOverlayView: PointsOverlayView? = null
    val storage by inject<IOAuth2AccessTokenStorage>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_decoder)
        mainLayout = findViewById<View>(R.id.main_layout) as ViewGroup
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            == PackageManager.PERMISSION_GRANTED
        ) {
            initQRCodeReaderView()
        } else {
            requestCameraPermission()
        }
    }

    override fun onResume() {
        super.onResume()
        if (qrCodeReaderView != null) {
            qrCodeReaderView!!.startCamera()
        }
    }

    override fun onPause() {
        super.onPause()
        if (qrCodeReaderView != null) {
            qrCodeReaderView!!.stopCamera()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        if (requestCode != MY_PERMISSION_REQUEST_CAMERA) {
            return
        }
        if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Snackbar.make(mainLayout!!, "L'autorisation de filmer a été accordée.", Snackbar.LENGTH_SHORT).show()
            initQRCodeReaderView()
        } else {
            Snackbar.make(mainLayout!!, "La demande d'autorisation pour la caméra a été refusée.", Snackbar.LENGTH_SHORT).show()
        }
    }

    // Called when a QR is decoded
    // "text" : the text encoded in QR
    // "points" : points where QR control points are placed
    override fun onQRCodeRead(text: String, points: Array<PointF>) {
        pointsOverlayView!!.setPoints(points)

        if (!text.isEmpty()) {

            storage.storeQR(text)

            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.CAMERA
            )
        ) {
            Snackbar.make(
                mainLayout!!,
                "L'accès à la caméra est nécessaire pour afficher l'aperçu de la caméra.",
                Snackbar.LENGTH_INDEFINITE
            ).setAction("OK") {
                ActivityCompat.requestPermissions(
                    this@DecoderActivity, arrayOf(
                        Manifest.permission.CAMERA
                    ), MY_PERMISSION_REQUEST_CAMERA
                )
            }.show()
        } else {
            Snackbar.make(
                mainLayout!!,
                "L'autorisation n'est pas disponible. Demande d'autorisation pour la caméra.",
                Snackbar.LENGTH_SHORT
            ).show()
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.CAMERA
                ), MY_PERMISSION_REQUEST_CAMERA
            )
        }
    }

    private fun initQRCodeReaderView() {
        val content = layoutInflater.inflate(R.layout.content_decoder, mainLayout, true)
        qrCodeReaderView = content.findViewById<View>(R.id.qrdecoderview) as QRCodeReaderView
        pointsOverlayView = content.findViewById<View>(R.id.points_overlay_view) as PointsOverlayView
        qrCodeReaderView!!.setAutofocusInterval(2000L)
        qrCodeReaderView!!.setOnQRCodeReadListener(this)
        qrCodeReaderView!!.setBackCamera()
        qrCodeReaderView!!.startCamera()
    }

    companion object {
        private const val MY_PERMISSION_REQUEST_CAMERA = 0
    }


}