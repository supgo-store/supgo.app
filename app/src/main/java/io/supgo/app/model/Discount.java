package io.supgo.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

public class Discount implements Parcelable {
    private long idDiscount;
    private String uuid;
    private long percentage;
    private String startDate;
    private String endDate;
    private String productUUID;
    private ChildProduct childProduct;
    private Object[] orders;


    protected Discount(Parcel in) {
        idDiscount = in.readLong();
        uuid = in.readString();
        percentage = in.readLong();
        startDate = in.readString();
        endDate = in.readString();
        productUUID = in.readString();
    }

    public static final Creator<Discount> CREATOR = new Creator<Discount>() {
        @Override
        public Discount createFromParcel(Parcel in) {
            return new Discount(in);
        }

        @Override
        public Discount[] newArray(int size) {
            return new Discount[size];
        }
    };

    @JsonProperty("idDiscount")
    public long getIDDiscount() { return idDiscount; }
    @JsonProperty("idDiscount")
    public void setIDDiscount(long value) { this.idDiscount = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("percentage")
    public long getPercentage() { return percentage; }
    @JsonProperty("percentage")
    public void setPercentage(long value) { this.percentage = value; }

    @JsonProperty("startDate")
    public String getStartDate() { return startDate; }
    @JsonProperty("startDate")
    public void setStartDate(String value) { this.startDate = value; }

    @JsonProperty("endDate")
    public String getEndDate() { return endDate; }
    @JsonProperty("endDate")
    public void setEndDate(String value) { this.endDate = value; }

    @JsonProperty("productUuid")
    public String getProductUUID() { return productUUID; }
    @JsonProperty("productUuid")
    public void setProductUUID(String value) { this.productUUID = value; }

    @JsonProperty("childProduct")
    public ChildProduct getChildProduct() { return childProduct; }
    @JsonProperty("childProduct")
    public void setChildProduct(ChildProduct value) { this.childProduct = value; }

    @JsonProperty("orders")
    public Object[] getOrders() { return orders; }
    @JsonProperty("orders")
    public void setOrders(Object[] value) { this.orders = value; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(idDiscount);
        parcel.writeString(uuid);
        parcel.writeLong(percentage);
        parcel.writeString(startDate);
        parcel.writeString(endDate);
        parcel.writeString(productUUID);
    }
}