package io.supgo.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChildStore {
    private long idStore;
    private String uuid;
    private String name;
    private String latitude;
    private String longitude;
    private Object[] cart;
    private Object[] stock;

    @JsonProperty("idStore")
    public long getIDStore() {
        return idStore;
    }

    @JsonProperty("idStore")
    public void setIDStore(long value) {
        this.idStore = value;
    }

    @JsonProperty("uuid")
    public String getUUID() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUUID(String value) {
        this.uuid = value;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String value) {
        this.name = value;
    }

    @JsonProperty("latitude")
    public String getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(String value) {
        this.latitude = value;
    }

    @JsonProperty("longitude")
    public String getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(String value) {
        this.longitude = value;
    }

    @JsonProperty("cart")
    public Object[] getCart() {
        return cart;
    }

    @JsonProperty("cart")
    public void setCart(Object[] value) {
        this.cart = value;
    }

    @JsonProperty("stock")
    public Object[] getStock() {
        return stock;
    }

    @JsonProperty("stock")
    public void setStock(Object[] value) {
        this.stock = value;
    }

}