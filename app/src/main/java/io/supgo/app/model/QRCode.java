package io.supgo.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QRCode {

    private String actionType;
    private String storeUuid;

    public QRCode(String actionType, String storeUuid) {
        this.actionType = actionType;
        this.storeUuid = storeUuid;
    }

    @JsonProperty("actionType")
    public String getActionType() { return actionType; }
    @JsonProperty("actionType")
    public void setActionType(String value) { this.actionType = value; }

    @JsonProperty("storeUuid")
    public String getStoreUuid() { return storeUuid; }
    @JsonProperty("storeUuid")
    public void setStoreUuid(String value) { this.storeUuid = value; }


}
