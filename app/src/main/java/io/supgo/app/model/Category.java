package io.supgo.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Category {

    private long idCategory;
    private String uuid;
    private String name;
    private String department;
    private Object[] product;

    public Category(long idCategory, String uuid, String name, String department, Object[] product) {
        this.idCategory = idCategory;
        this.uuid = uuid;
        this.name = name;
        this.department = department;
        this.product = product;
    }

    @JsonProperty("idCategory")
    public long getIDCategory() { return idCategory; }
    @JsonProperty("idCategory")
    public void setIDCategory(long value) { this.idCategory = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("department")
    public String getDepartment() { return department; }
    @JsonProperty("department")
    public void setDepartment(String value) { this.department = value; }

    @JsonProperty("product")
    public Object[] getProduct() { return product; }
    @JsonProperty("product")
    public void setProduct(Object[] value) { this.product = value; }
}
