package io.supgo.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Comment {
    private long idComment;
    private String uuid;
    private String datetime;
    private long nbStars;
    private String text;
    private String productUUID;
    private String userUUID;
    private ChildProduct childProduct;
    private ChildUser childUser;

    public Comment(long nbStars, String text, String productUUID, String userUUID) {
        this.nbStars = nbStars;
        this.text = text;
        this.productUUID = productUUID;
        this.userUUID = userUUID;
    }

    @JsonProperty("idComment")
    public long getIDComment() { return idComment; }
    @JsonProperty("idComment")
    public void setIDComment(long value) { this.idComment = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("datetime")
    public String getDatetime() { return datetime; }
    @JsonProperty("datetime")
    public void setDatetime(String value) { this.datetime = value; }

    @JsonProperty("nbStars")
    public long getNbStars() { return nbStars; }
    @JsonProperty("nbStars")
    public void setNbStars(long value) { this.nbStars = value; }

    @JsonProperty("text")
    public String getText() { return text; }
    @JsonProperty("text")
    public void setText(String value) { this.text = value; }

    @JsonProperty("productUuid")
    public String getProductUUID() { return productUUID; }
    @JsonProperty("productUuid")
    public void setProductUUID(String value) { this.productUUID = value; }

    @JsonProperty("userUuid")
    public String getUserUUID() { return userUUID; }
    @JsonProperty("userUuid")
    public void setUserUUID(String value) { this.userUUID = value; }

    @JsonProperty("childProduct")
    public ChildProduct getChildProduct() { return childProduct; }
    @JsonProperty("childProduct")
    public void setChildProduct(ChildProduct value) { this.childProduct = value; }

    @JsonProperty("childUser")
    public ChildUser getChildUser() { return childUser; }
    @JsonProperty("childUser")
    public void setChildUser(ChildUser value) { this.childUser = value; }
}
