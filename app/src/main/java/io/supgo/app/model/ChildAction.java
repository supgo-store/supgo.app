package io.supgo.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChildAction {
    private long idAction;
    private String uuid;
    private String actionType;
    private Object[] history;

    @JsonProperty("idAction")
    public long getIDAction() { return idAction; }
    @JsonProperty("idAction")
    public void setIDAction(long value) { this.idAction = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("actionType")
    public String getActionType() { return actionType; }
    @JsonProperty("actionType")
    public void setActionType(String value) { this.actionType = value; }

    @JsonProperty("history")
    public Object[] getHistory() { return history; }
    @JsonProperty("history")
    public void setHistory(Object[] value) { this.history = value; }
}