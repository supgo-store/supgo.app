package io.supgo.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;

public class History {

    private long idHistory;
    private String uuid;
    private OffsetDateTime datetime;
    private String actionUUID;
    private Object productUUID;
    private String userUUID;
    private String storeUUID;
    private ChildAction childAction;
    private Object childProduct;
    private ChildStore childStore;
    private ChildUser childUser;

    public History(String actionUUID, Object productUUID, String userUUID, String storeUUID) {
        this.actionUUID = actionUUID;
        this.productUUID = productUUID;
        this.userUUID = userUUID;
        this.storeUUID = storeUUID;
    }

    @JsonProperty("idHistory")
    public long getIDHistory() { return idHistory; }
    @JsonProperty("idHistory")
    public void setIDHistory(long value) { this.idHistory = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("datetime")
    public OffsetDateTime getDatetime() { return datetime; }
    @JsonProperty("datetime")
    public void setDatetime(OffsetDateTime value) { this.datetime = value; }

    @JsonProperty("actionUuid")
    public String getActionUUID() { return actionUUID; }
    @JsonProperty("actionUuid")
    public void setActionUUID(String value) { this.actionUUID = value; }

    @JsonProperty("productUuid")
    public Object getProductUUID() { return productUUID; }
    @JsonProperty("productUuid")
    public void setProductUUID(Object value) { this.productUUID = value; }

    @JsonProperty("userUuid")
    public String getUserUUID() { return userUUID; }
    @JsonProperty("userUuid")
    public void setUserUUID(String value) { this.userUUID = value; }

    @JsonProperty("storeUuid")
    public String getStoreUUID() { return storeUUID; }
    @JsonProperty("storeUuid")
    public void setStoreUUID(String value) { this.storeUUID = value; }

    @JsonProperty("childAction")
    public ChildAction getChildAction() { return childAction; }
    @JsonProperty("childAction")
    public void setChildAction(ChildAction value) { this.childAction = value; }

    @JsonProperty("childProduct")
    public Object getChildProduct() { return childProduct; }
    @JsonProperty("childProduct")
    public void setChildProduct(Object value) { this.childProduct = value; }

    @JsonProperty("childStore")
    public ChildStore getChildStore() { return childStore; }
    @JsonProperty("childStore")
    public void setChildStore(ChildStore value) { this.childStore = value; }

    @JsonProperty("childUser")
    public ChildUser getChildUser() { return childUser; }
    @JsonProperty("childUser")
    public void setChildUser(ChildUser value) { this.childUser = value; }
}