package io.supgo.app.model;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {

    private long idProduct;
    private String uuid;
    private String name;
    private double price;
    private String description;
    private String categoryUUID;
    private String image;
    private ChildCategory childCategory;
    private Object[] comment;
    private Object[] discount;
    private Object[] history;
    private Object[] orders;
    private Object[] stock;
    private String percentage;

    public Product(String name, double price, String image) {
        this.name = name;
        this.price = price;
        this.image = image;
    }

    @JsonProperty("idProduct")
    public long getIDProduct() { return idProduct; }
    @JsonProperty("idProduct")
    public void setIDProduct(long value) { this.idProduct = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("price")
    public double getPrice() { return price; }
    @JsonProperty("price")
    public void setPrice(double value) { this.price = value; }

    @JsonProperty("description")
    public String getDescription() { return description; }
    @JsonProperty("description")
    public void setDescription(String value) { this.description = value; }

    @JsonProperty("categoryUuid")
    public String getCategoryUUID() { return categoryUUID; }
    @JsonProperty("categoryUuid")
    public void setCategoryUUID(String value) { this.categoryUUID = value; }

    @JsonProperty("image")
    public String getImage() { return image; }
    @JsonProperty("image")
    public void setImage(String value) { this.image = value; }

    @JsonProperty("childCategory")
    public ChildCategory getChildCategory() { return childCategory; }
    @JsonProperty("childCategory")
    public void setChildCategory(ChildCategory value) { this.childCategory = value; }

    public String getPercentage() {
        return percentage;
    }
    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }


    @JsonProperty("comment")
    public Object[] getComment() { return comment; }
    @JsonProperty("comment")
    public void setComment(Object[] value) { this.comment = value; }

    @JsonProperty("discount")
    public Object[] getDiscount() { return discount; }
    @JsonProperty("discount")
    public void setDiscount(Object[] value) { this.discount = value; }

    @JsonProperty("history")
    public Object[] getHistory() { return history; }
    @JsonProperty("history")
    public void setHistory(Object[] value) { this.history = value; }

    @JsonProperty("orders")
    public Object[] getOrders() { return orders; }
    @JsonProperty("orders")
    public void setOrders(Object[] value) { this.orders = value; }

    @JsonProperty("stock")
    public Object[] getStock() { return stock; }
    @JsonProperty("stock")
    public void setStock(Object[] value) { this.stock = value; }

}
