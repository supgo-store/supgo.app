package io.supgo.app.model;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cart {

    private String idCart;
    private String uuid;
    private String userUUID;
    private String storeUUID;
    private String datetime;
    private String totalPrice;
    private ChildStore childStore;
    private ChildUser childUser;
    private Object[] orders;

    public Cart(String uuid, String userUUID, String storeUUID, String datetime, String totalPrice) {
        this.uuid = uuid;
        this.userUUID = userUUID;
        this.storeUUID = storeUUID;
        this.datetime = datetime;
        this.totalPrice = totalPrice;
    }

    @JsonProperty("idCart")
    public String getIDCart() { return idCart; }
    @JsonProperty("idCart")
    public void setIDCart(String value) { this.idCart = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("userUuid")
    public String getUserUUID() { return userUUID; }
    @JsonProperty("userUuid")
    public void setUserUUID(String value) { this.userUUID = value; }

    @JsonProperty("storeUuid")
    public String getStoreUUID() { return storeUUID; }
    @JsonProperty("storeUuid")
    public void setStoreUUID(String value) { this.storeUUID = value; }

    @JsonProperty("datetime")
    public String getDatetime() { return datetime; }
    @JsonProperty("datetime")
    public void setDatetime(String value) { this.datetime = value; }

    @Nullable
    @JsonProperty("totalPrice")
    public String getTotalPrice() { return  totalPrice; }
    @Nullable
    @JsonProperty("totalPrice")
    public void setTotalPrice(String value) { this.totalPrice = value; }

    @JsonProperty("childStore")
    public ChildStore getChildStore() { return childStore; }
    @JsonProperty("childStore")
    public void setChildStore(ChildStore value) { this.childStore = value; }

    @JsonProperty("childUser")
    public ChildUser getChildUser() { return childUser; }

    @JsonProperty("childUser")
    public void setChildUser(ChildUser value) { this.childUser = value; }

    @JsonProperty("orders")
    public Object[] getOrders() { return orders; }
    @JsonProperty("orders")
    public void setOrders(Object[] value) { this.orders = value; }
}