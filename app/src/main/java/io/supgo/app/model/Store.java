package io.supgo.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Store {

    private long idStore;
    private String uuid;
    private String name;
    private String latitude;
    private String longitude;
    private List<Object> stock;

    public Store(long idStore, String uuid, String name, String latitude, String longitude, List<Object> stock) {
        this.idStore = idStore;
        this.uuid = uuid;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.stock = stock;
    }

    @JsonProperty("idStore")
    public long getIDStore() { return idStore; }
    @JsonProperty("idStore")
    public void setIDStore(long value) { this.idStore = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("latitude")
    public String getLatitude() { return latitude; }
    @JsonProperty("latitude")
    public void setLatitude(String value) { this.latitude = value; }

    @JsonProperty("longitude")
    public String getLongitude() { return longitude; }
    @JsonProperty("longitude")
    public void setLongitude(String value) { this.longitude = value; }

    @JsonProperty("stock")
    public List<Object> getStock() { return stock; }
    @JsonProperty("stock")
    public void setStock(List<Object> value) { this.stock = value; }
}