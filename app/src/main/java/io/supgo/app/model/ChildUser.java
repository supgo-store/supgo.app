package io.supgo.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChildUser {
    private long idUser;
    private String firstname;
    private String lastname;
    private String email;
    private String uuid;
    private Object cardNumber;
    private Object cardExpireDate;
    private String cardOwner;
    private Object cardCcv;
    private Object address;
    private Object city;
    private Object postalCode;
    private Object phone;
    private Object birthdate;
    private Object gender;
    private Object[] cart;
    private Object[] comment;
    private Object[] history;

    @JsonProperty("idUser")
    public long getIDUser() { return idUser; }
    @JsonProperty("idUser")
    public void setIDUser(long value) { this.idUser = value; }

    @JsonProperty("firstname")
    public String getFirstname() { return firstname; }
    @JsonProperty("firstname")
    public void setFirstname(String value) { this.firstname = value; }

    @JsonProperty("lastname")
    public String getLastname() { return lastname; }
    @JsonProperty("lastname")
    public void setLastname(String value) { this.lastname = value; }

    @JsonProperty("email")
    public String getEmail() { return email; }
    @JsonProperty("email")
    public void setEmail(String value) { this.email = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("cardNumber")
    public Object getCardNumber() { return cardNumber; }
    @JsonProperty("cardNumber")
    public void setCardNumber(Object value) { this.cardNumber = value; }

    @JsonProperty("cardExpireDate")
    public Object getCardExpireDate() { return cardExpireDate; }
    @JsonProperty("cardExpireDate")
    public void setCardExpireDate(Object value) { this.cardExpireDate = value; }

    @JsonProperty("cardOwner")
    public String getCardOwner() { return cardOwner; }
    @JsonProperty("cardOwner")
    public void setCardOwner(String value) { this.cardOwner = value; }

    @JsonProperty("cardCcv")
    public Object getCardCcv() { return cardCcv; }
    @JsonProperty("cardCcv")
    public void setCardCcv(Object value) { this.cardCcv = value; }

    @JsonProperty("address")
    public Object getAddress() { return address; }
    @JsonProperty("address")
    public void setAddress(Object value) { this.address = value; }

    @JsonProperty("city")
    public Object getCity() { return city; }
    @JsonProperty("city")
    public void setCity(Object value) { this.city = value; }

    @JsonProperty("postalCode")
    public Object getPostalCode() { return postalCode; }
    @JsonProperty("postalCode")
    public void setPostalCode(Object value) { this.postalCode = value; }

    @JsonProperty("phone")
    public Object getPhone() { return phone; }
    @JsonProperty("phone")
    public void setPhone(Object value) { this.phone = value; }

    @JsonProperty("birthdate")
    public Object getBirthdate() { return birthdate; }
    @JsonProperty("birthdate")
    public void setBirthdate(Object value) { this.birthdate = value; }

    @JsonProperty("gender")
    public Object getGender() { return gender; }
    @JsonProperty("gender")
    public void setGender(Object value) { this.gender = value; }

    @JsonProperty("cart")
    public Object[] getCart() { return cart; }
    @JsonProperty("cart")
    public void setCart(Object[] value) { this.cart = value; }

    @JsonProperty("comment")
    public Object[] getComment() { return comment; }
    @JsonProperty("comment")
    public void setComment(Object[] value) { this.comment = value; }

    @JsonProperty("history")
    public Object[] getHistory() { return history; }
    @JsonProperty("history")
    public void setHistory(Object[] value) { this.history = value; }
}
