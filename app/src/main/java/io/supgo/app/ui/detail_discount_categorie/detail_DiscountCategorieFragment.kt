package io.supgo.app.ui.detail_discount_categorie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.supgo.app.RequestManager
import io.supgo.app.UtilRequest.getToken
import io.supgo.app.model.Cart
import io.supgo.app.model.Discount
import io.supgo.keycloaker.R
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDate.parse
import java.time.LocalDateTime
import java.util.*
import java.util.logging.Level.parse


class detail_DiscountCategorieFragment : Fragment() {

    var discount: List<Discount>? = null
    var recyclerView: RecyclerView? = null
    var requestDiscount: RequestManager? = null
    val tmp_searchedDiscount = ArrayList<Discount>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_detail_categorie, container, false)
        (requireActivity() as AppCompatActivity).supportActionBar!!.setTitle(arguments!!.getString("nom_detail_categorie"))

        requestDiscount = RequestManager()

        try {
            initializeDataDiscount()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        //Filter
        tmp_searchedDiscount.clear()
        var date_start: LocalDateTime
        var date_end: LocalDateTime
        var date_current = LocalDateTime.now()
        for(searchedValidDiscount in discount!!){

            date_start = LocalDateTime.parse(searchedValidDiscount.startDate)
            date_end = LocalDateTime.parse(searchedValidDiscount.endDate)

            if(date_current.isAfter(date_start) && date_current.isBefore(date_end)){
                tmp_searchedDiscount.add(searchedValidDiscount)
            }


        }

        recyclerView = root.findViewById(R.id.recyclerview_detail_categorie)
        val llm = LinearLayoutManager(this.context)
        recyclerView!!.setLayoutManager(llm)
        val adapter = detail_DiscountCategorieAdapter(tmp_searchedDiscount)
        recyclerView!!.setAdapter(adapter)

        return root
    }

    private fun initializeDataDiscount() {

        discount = ArrayList()
        val listType = object : TypeToken<ArrayList<Discount?>?>() {}.type
        val discount_tmp = Gson().fromJson<ArrayList<Discount>>(

            requestDiscount!!.getDiscount(getToken()), listType)

        discount = discount_tmp.clone() as List<Discount>

    }
}