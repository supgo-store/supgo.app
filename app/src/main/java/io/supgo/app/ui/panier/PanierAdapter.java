package io.supgo.app.ui.panier;

import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import io.supgo.app.model.Product;
import io.supgo.keycloaker.R;

public class PanierAdapter extends RecyclerView.Adapter<PanierViewHolder> {

    public List<Product> listeProduits;


    public PanierAdapter(List<Product> produits) {
        this.listeProduits = produits;
    }

    @NonNull
    @Override
    public PanierViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_detail_discount_categorie_layout, parent, false);
        PanierViewHolder PanierViewHolder = new PanierViewHolder(view);

        return PanierViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final PanierViewHolder holder, int position) {

        holder.nom.setText(listeProduits.get(position).getName());
        holder.prix.setText(listeProduits.get(position).getPrice() + " €");
        holder.description.setText(listeProduits.get(position).getDescription());
        holder.uuid.setText(listeProduits.get(position).getUUID());

        if(listeProduits.get(position).getPercentage() == ""){
            holder.pourcentage.setText("");
        }else {
            holder.pourcentage.setText("- " + listeProduits.get(position).getPercentage() + "  %");
        }

        byte[] imageBytes = Base64.decode(listeProduits.get(position).getImage(), Base64.DEFAULT);
        holder.image.setImageBitmap(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length));






    }

    @Override
    public int getItemCount() {
        return this.listeProduits.size();
    }



}
