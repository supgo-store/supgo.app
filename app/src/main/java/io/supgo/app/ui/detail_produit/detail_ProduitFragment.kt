package io.supgo.app.ui.detail_produit

import android.app.Dialog
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.supgo.app.RequestManager
import io.supgo.app.UtilRequest.getToken
import io.supgo.app.UtilRequest.getUserId
import io.supgo.app.model.Comment
import io.supgo.keycloaker.R
import java.io.IOException
import java.util.*
import java.util.zip.Inflater


class detail_ProduitFragment : Fragment(){

    var comment: List<Comment>? = null
    var recyclerView: RecyclerView? = null
    var requestComments: RequestManager? = null
    var myDialog: Dialog? = null
    var uuid: String? = null
    val tmp_searchedComment = ArrayList<Comment>()
    var root: View? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        root = inflater.inflate(R.layout.fragment_detail_produit, container, false)
        uuid = arguments!!.getString("uuid_detail_produit").toString()
        myDialog = Dialog(context)

        myDialog!!.setContentView(R.layout.popup_monprofil)

        requestComments  = RequestManager()

        try {
            initializeDataComment()
        } catch (e: IOException) {
            e.printStackTrace()
        }


        var nom_detail: TextView
        nom_detail = root!!.findViewById(R.id.txt_nom_detail_produit)
        nom_detail.text = arguments!!.get("nom_detail_produit").toString()

        var description_detail: TextView
        description_detail = root!!.findViewById(R.id.txt_description_detail_produit)
        description_detail.text = arguments!!.get("description_detail_produit").toString()

        var prix_detail: TextView
        prix_detail = root!!.findViewById(R.id.txt_prix_detail_produit)
        prix_detail.text = arguments!!.get("prix_detail_produit").toString()


        var image_detail: ImageView
        var bmp: Bitmap = BitmapFactory.decodeByteArray(arguments!!.getByteArray("image_detail_produit"), 0, arguments!!.getByteArray("image_detail_produit").size)
        image_detail = root!!.findViewById(R.id.image_detail_produit)
        image_detail.setImageBitmap(bmp)

        var ajout_avis_detail: TextView
        ajout_avis_detail = root!!.findViewById(R.id.txt_ajout_avis_detail_produit)
        ajout_avis_detail.setOnClickListener { ShowPopupCommentaire() }


        tmp_searchedComment.clear()

        for(searchedComment in comment!!){

            if(searchedComment.childProduct.uuid == uuid){
                tmp_searchedComment.add(searchedComment)
            }
        }

        recyclerView = root!!.findViewById(R.id.recyclerview_detail_produit)
        val llm = LinearLayoutManager(this.context)
        recyclerView!!.setLayoutManager(llm)
        val adapter = detail_ProduitAdapter(tmp_searchedComment)
        recyclerView!!.setAdapter(adapter)


        return root
    }

    private fun initializeDataComment() {

        comment = ArrayList()
        val listType = object : TypeToken<ArrayList<Comment?>?>() {}.type
        val comments = Gson().fromJson<ArrayList<Comment>>(

            requestComments!!.getComment(getToken()), listType)

        comment = comments.clone() as List<Comment>

    }

    fun ShowPopupCommentaire() {
        val imageClose: ImageView
        val buttonSave: Button
        val ratingBar: RatingBar
        val commentaire: TextView

        myDialog!!.setContentView(R.layout.popup_commentaire)

        buttonSave = myDialog!!.findViewById(R.id.btn_save_card_monpaiement)
        imageClose = myDialog!!.findViewById(R.id.image_close)
        ratingBar = myDialog!!.findViewById(R.id.ratingBar_commentaire)
        commentaire = myDialog!!.findViewById(R.id.txt_commentaire)

        imageClose.setOnClickListener { myDialog!!.dismiss() }
        buttonSave.setOnClickListener {
            CallbackSaveComment(ratingBar.numStars, commentaire.text.toString())
            myDialog!!.dismiss() }

        myDialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog!!.show()
    }


    fun CallbackSaveComment(numStars: Int, commentaire: String) {

        var progressBar_produit: ProgressBar
        progressBar_produit = root!!.findViewById(R.id.progressBar_produit)
        progressBar_produit.visibility = View.VISIBLE

        var sendComment: Comment? = Comment(numStars.toLong(), commentaire,  uuid, getUserId())

        val response = requestComments!!.setComment(getToken(), Gson().toJson(sendComment))


        if(response){

            val ft = fragmentManager!!.beginTransaction()
            if (Build.VERSION.SDK_INT >= 26) {
                ft.setReorderingAllowed(false)
            }
            ft.detach(this).attach(this).commit()

            Toast.makeText(context, "Commentaire publier avec succès !", Toast.LENGTH_LONG).show()

        }


    }

}