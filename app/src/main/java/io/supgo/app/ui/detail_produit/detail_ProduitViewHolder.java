package io.supgo.app.ui.detail_produit;

import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;

import io.supgo.keycloaker.R;

public class detail_ProduitViewHolder extends RecyclerView.ViewHolder {

    CardView cardView;
    TextView nom;
    TextView date;
    RatingBar note;
    TextView commentaire;

    public detail_ProduitViewHolder(View view){
        super(view);

        cardView = view.findViewById(R.id.card_defaut_liste_comment_produit);
        nom = view.findViewById(R.id.txt_nom_cardview_detail_comment_produit);
        date = view.findViewById(R.id.date_cardview_detail_comment_produit);
        note = view.findViewById(R.id.rating_bar_carview_detail_comment_produit);
        commentaire = view.findViewById(R.id.txt_commentaire_cardview_detail_comment_produit);

    }

}
