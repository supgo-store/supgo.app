package io.supgo.app.ui.historique;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.supgo.app.model.Cart;
import io.supgo.app.model.Category;

import io.supgo.app.ui.nosproduits.CategorieViewHolder;
import io.supgo.keycloaker.R;

public class HistoriqueAdapter extends RecyclerView.Adapter<HistoriqueViewHolder> {

    public List<Cart> listeCart;

    public HistoriqueAdapter(List<Cart> carts){
        this.listeCart = carts;
    }

    @NonNull
    @Override
    public HistoriqueViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_historique_layout, parent, false);
        HistoriqueViewHolder historiqueViewHolder = new HistoriqueViewHolder(view);

        return historiqueViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoriqueViewHolder holder, int position) {

        holder.ville.setText(listeCart.get(position).getChildStore().getName());

        String date[] = listeCart.get(position).getDatetime().split("-");
        String tmp_date[] = date[2].split("T");
        holder.date.setText( tmp_date[0] + "/" + date[1] + "/" + date[0] + "  " + tmp_date[1]);
        holder.numero_carte.setText(listeCart.get(position).getChildUser().getCardNumber().toString());
        holder.prixTotal.setText((listeCart.get(position).getTotalPrice() == null) ? "0 €" : listeCart.get(position).getTotalPrice() + " €");

    }

    @Override
    public int getItemCount() {
        return this.listeCart.size();
    }



}
