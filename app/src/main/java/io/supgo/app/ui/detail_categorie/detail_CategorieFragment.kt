package io.supgo.app.ui.detail_categorie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.supgo.app.RequestManager
import io.supgo.app.UtilRequest
import io.supgo.app.UtilRequest.getToken
import io.supgo.app.model.Discount
import io.supgo.app.model.Product
import io.supgo.app.model.Store
import io.supgo.app.ui.detail_discount_categorie.detail_DiscountCategorieAdapter
import io.supgo.app.ui.magasin.MagasinAdapter
import io.supgo.keycloaker.R
import java.io.IOException
import java.util.ArrayList

class detail_CategorieFragment : Fragment() {

    var product: List<Product>? = null
    var recyclerView: RecyclerView? = null
    var requestProduits: RequestManager? = null



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_detail_categorie, container, false)
        val category_name =  arguments!!.getString("nom_detail_categorie")
        (requireActivity() as AppCompatActivity).supportActionBar!!.setTitle(category_name)



        requestProduits  = RequestManager()

        try {
                initializeDataProduct()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val tmp_searchedProduct = ArrayList<Product>()

            for (searchedProduct in product!!) {
                if(searchedProduct.childCategory.name == category_name){
                    tmp_searchedProduct.add(searchedProduct)
                }
            }

        recyclerView = root.findViewById(R.id.recyclerview_detail_categorie)
        val llm = LinearLayoutManager(this.context)
        recyclerView!!.setLayoutManager(llm)
        val adapter = detail_CategorieAdapter(tmp_searchedProduct)
        recyclerView!!.setAdapter(adapter)

        return root
    }

    private fun initializeDataProduct() {

        product = ArrayList()
        val listType = object : TypeToken<ArrayList<Product?>?>() {}.type
        val products = Gson().fromJson<ArrayList<Product>>(

            requestProduits!!.getProduct(getToken()), listType)

        product = products.clone() as List<Product>

    }

}