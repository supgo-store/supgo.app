package io.supgo.app.ui.panier

import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.supgo.app.RequestManager
import io.supgo.app.UtilRequest.getToken
import io.supgo.app.model.Discount
import io.supgo.app.model.Product
import io.supgo.app.ui.detail_categorie.detail_CategorieAdapter
import io.supgo.app.ui.detail_discount_categorie.detail_DiscountCategorieAdapter
import io.supgo.app.ui.detail_discount_categorie.detail_DiscountCategorieFragment
import io.supgo.keycloaker.R
import io.supgo.keycloaker.storage.IOAuth2AccessTokenStorage
import org.koin.android.ext.android.inject
import java.io.IOException
import java.time.LocalDateTime
import java.util.*
import java.util.stream.IntStream
import kotlin.concurrent.schedule


class PanierFragment : Fragment() {

    val storage by inject<IOAuth2AccessTokenStorage>()
    var txt_panier: TextView? = null
    var img_panier: ImageView? = null
    var layout_panier: LinearLayout? = null
    var product: List<Product>? = null
    var discount: List<Discount>? = null
    var recyclerView: RecyclerView? = null
    var prix_panier: TextView? = null
    val tmp_searchedDiscount = ArrayList<Discount>()
    val tmp_validProduct = ArrayList<Discount>()
    var total_prix_panier = 0.0
    var requestProduct: RequestManager? = null



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root =  inflater.inflate(R.layout.fragment_panier, container, false)

        txt_panier = root.findViewById(R.id.txt_panier)
        img_panier = root.findViewById(R.id.img_panier)
        layout_panier = root.findViewById(R.id.linearLayout_panier)
        prix_panier = root.findViewById(R.id.txt_prix_panier)

        requestProduct = RequestManager()
        total_prix_panier = 0.0

        if(storage.getStoredStore() != "" && storage.getStoredQR() != ""){

            //txt_panier!!.text = "Le panier est vide pour le moment."

            try {
                initializeDataProduct()
                initializeDataDiscount()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            tmp_searchedDiscount.clear()
            tmp_validProduct.clear()

            var date_start: LocalDateTime
            var date_end: LocalDateTime
            var date_current = LocalDateTime.now()
            for(searchedValidDiscount in discount!!){

                date_start = LocalDateTime.parse(searchedValidDiscount.startDate)
                date_end = LocalDateTime.parse(searchedValidDiscount.endDate)

                if(date_current.isAfter(date_start) && date_current.isBefore(date_end)){
                    tmp_searchedDiscount.add(searchedValidDiscount)
                }

            }

        }

        if(product.isNullOrEmpty()){

            txt_panier!!.visibility = View.VISIBLE
            img_panier!!.visibility = View.VISIBLE
            layout_panier!!.visibility = View.GONE

        }else{

            txt_panier!!.visibility = View.GONE
            img_panier!!.visibility = View.GONE
            layout_panier!!.visibility = View.VISIBLE

            var reduction = 0.0
            var extraction_pourcentage = ""

            for (addPrice in product!!) {

                for (searchDisount in tmp_searchedDiscount!!){

                    if (addPrice.uuid == searchDisount.childProduct.uuid){

                        reduction = (addPrice.price - (addPrice.price * (searchDisount.percentage / 100.00)))
                        extraction_pourcentage = searchDisount.percentage.toString()

                    }else{
                        addPrice?.percentage = ""

                    }

                }

                if(reduction.equals(0.0)){
                    total_prix_panier = addPrice.price + total_prix_panier
                }else{
                    total_prix_panier = reduction + total_prix_panier
                }

                reduction = 0.0

                addPrice.percentage = extraction_pourcentage
                extraction_pourcentage = ""

            }

            prix_panier!!.text = "Total : " + total_prix_panier.toString() + " €"
            storage.storeFinalPrice(total_prix_panier.toString())
            recyclerView = root.findViewById(R.id.recyclerview_panier)
            val llm = LinearLayoutManager(this.context)
            recyclerView!!.setLayoutManager(llm)
            val adapter = PanierAdapter(product)
            recyclerView!!.setAdapter(adapter)

        }

        return root
    }

    private fun initializeDataProduct() {

        product = ArrayList()

        val listType = object : TypeToken<ArrayList<Product?>?>() {}.type
        val ProductsList =
            Gson().fromJson<ArrayList<Product>>( requestProduct!!.getProduct(getToken()), listType )

        product = ProductsList.clone() as List<Product>

    }

    private fun initializeDataDiscount() {

        discount = ArrayList()

        val listType = object : TypeToken<ArrayList<Discount?>?>() {}.type
        val DiscountsList =
            Gson().fromJson<ArrayList<Discount>>( requestProduct!!.getDiscount(getToken()), listType )

        discount = DiscountsList.clone() as List<Discount>

    }



}

