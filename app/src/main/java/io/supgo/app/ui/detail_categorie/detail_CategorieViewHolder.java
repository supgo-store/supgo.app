package io.supgo.app.ui.detail_categorie;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.io.ByteArrayOutputStream;

import io.supgo.keycloaker.R;

public class detail_CategorieViewHolder extends RecyclerView.ViewHolder {

    CardView cardView;
    TextView nom;
    TextView prix;
    TextView description;
    ImageView image;
    TextView uuid;

    public detail_CategorieViewHolder(@NonNull View view) {
        super(view);

        cardView = view.findViewById(R.id.card_defaut_liste_produit_categorie);
        nom = view.findViewById(R.id.txt_nom_cardview_detail_produit);
        prix = view.findViewById(R.id.txt_prix_cardview_detail_produit);
        image = view.findViewById(R.id.image_cardview_detail_produit);
        description = view.findViewById(R.id.txt_description_cardview_detail_produit);
        uuid = view.findViewById(R.id.txt_uuid_cardview_detail_produit);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle_product = new Bundle();
                bundle_product.putString("nom_detail_produit", nom.getText().toString());
                bundle_product.putString("description_detail_produit", description.getText().toString());
                bundle_product.putString("prix_detail_produit", prix.getText().toString());
                bundle_product.putString("uuid_detail_produit", uuid.getText().toString());

                    BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    drawable.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);

                bundle_product.putByteArray("image_detail_produit", stream.toByteArray());



                Navigation.findNavController(v).navigate(R.id.nav_detail_produit, bundle_product);

            }
        });


    }
}
