package io.supgo.app.ui.moncompte

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.supgo.app.RequestManager
import io.supgo.app.UtilRequest
import io.supgo.app.UtilRequest.getToken
import io.supgo.app.UtilRequest.getUserId
import io.supgo.app.model.User
import io.supgo.keycloaker.R
import io.supgo.keycloaker.helper.Helper
import io.supgo.keycloaker.storage.IOAuth2AccessTokenStorage
import org.koin.android.ext.android.inject

class MonCompteFragment : Fragment() {
    var myDialog: Dialog? = null
    var requestUser: RequestManager? = null
    var requestInfoUser: RequestManager? = null
    var info_users: User? = null
    var uuid: String? = null
    private val storage by inject<IOAuth2AccessTokenStorage>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_moncompte, container, false)

        myDialog = Dialog(context)
        requestUser = RequestManager()
        requestInfoUser = RequestManager()
        uuid = getUserId()

        initializeDataUser()

        val card_connexion_moncompte: CardView = root.findViewById(R.id.card_monprofil_moncompte)
        card_connexion_moncompte.setOnClickListener { ShowPopupMonProfil() }

        val card_creation_compte_moncompte: CardView = root.findViewById(R.id.card_monpaiement_moncompte)
        card_creation_compte_moncompte.setOnClickListener { ShowPopupMonPaiement() }

        return root
    }

    fun ShowPopupMonProfil() {
        val imageClose: ImageView
        val buttonSave: Button
        val txt_name: TextView
        val txt_email: TextView
        val txt_genre: TextView
        val txt_adresse: TextView
        val txt_ville: TextView
        val txt_code_postal: TextView
        val txt_telephone: TextView
        val txt_date_anniversaire: TextView


        val token = UtilRequest.storage.getStoredAccessToken()

        myDialog!!.setContentView(R.layout.popup_monprofil)
        imageClose = myDialog!!.findViewById(R.id.image_close)
        buttonSave = myDialog!!.findViewById(R.id.btn_save_card_monprofil)
        txt_name = myDialog!!.findViewById(R.id.txt_name_popup_monprofil)
        txt_email = myDialog!!.findViewById(R.id.txt_email_popup_monprofil)
        txt_genre = myDialog!!.findViewById(R.id.txt_genre_popup_monprofil)
        txt_adresse = myDialog!!.findViewById(R.id.txt_adresse_popup_monprofil)
        txt_ville = myDialog!!.findViewById(R.id.txt_ville_popup_monprofil)
        txt_code_postal = myDialog!!.findViewById(R.id.txt_code_postal_popup_monprofil)
        txt_telephone = myDialog!!.findViewById(R.id.txt_telephone_popup_monprofil)
        txt_date_anniversaire = myDialog!!.findViewById(R.id.txt_anniversaire_popup_monprofil)


        token?.apply {

            val principal = Helper.parseJwtToken(accessToken!!)
            txt_name.text = "${principal.name} ${principal.surname}"
            txt_email.text = principal.email
        }

        if(info_users!!.address != null && info_users!!.city != null && info_users!!.postalCode != null && info_users!!.phone != null && info_users!!.birthdate != null && info_users!!.gender != null ){
            txt_adresse!!.text = info_users!!.address.toString()
            txt_ville!!.text = info_users!!.city.toString()
            txt_code_postal.text = info_users!!.postalCode.toString()
            txt_telephone.text = info_users!!.phone.toString()
            txt_date_anniversaire.text = info_users!!.birthdate.toString()
            txt_genre.text = info_users!!.gender.toString()
        }


        imageClose.setOnClickListener { myDialog!!.dismiss() }
        buttonSave.setOnClickListener {
            CallbackSaveProfil(txt_adresse?.text.toString(), txt_ville?.text.toString(), txt_code_postal?.text.toString(), txt_telephone?.text.toString(), txt_date_anniversaire.text.toString(), txt_genre.text.toString() )
            myDialog!!.dismiss()
        }

        myDialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog!!.show()
    }

    fun ShowPopupMonPaiement() {
        val imageClose: ImageView
        val buttonSave: Button
        val numCarte: TextView
        val expCarte: TextView
        val codeCarte: TextView

        myDialog!!.setContentView(R.layout.popup_monpaiement)

        buttonSave = myDialog!!.findViewById(R.id.btn_save_card_monpaiement)
        imageClose = myDialog!!.findViewById(R.id.image_close)
        numCarte = myDialog!!.findViewById(R.id.txt_numero_carte)
        expCarte = myDialog!!.findViewById(R.id.txt_expiration_carte)
        codeCarte = myDialog!!.findViewById(R.id.txt_code_carte)

        if(info_users!!.cardExpireDate != null && !info_users!!.cardCcv.isEmpty() && !info_users!!.cardNumber.isEmpty() && !info_users!!.cardOwner.isEmpty()){
            numCarte!!.text = info_users!!.cardNumber
            expCarte!!.text = info_users!!.cardExpireDate
            codeCarte!!.text = info_users!!.cardCcv
        }


        imageClose.setOnClickListener { myDialog!!.dismiss() }
        buttonSave.setOnClickListener {
            CallbackSaveCard(numCarte?.text.toString(), expCarte?.text.toString(), codeCarte?.text.toString())
            myDialog!!.dismiss()
        }

        myDialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog!!.show()
    }

    fun CallbackSaveCard(numeroCarte: String, expirationCarte: String, codeCarte: String){

        info_users!!.cardNumber = numeroCarte
        info_users!!.cardExpireDate = expirationCarte
        info_users!!.cardCcv = codeCarte
        info_users!!.cardOwner = (info_users!!.lastname).toUpperCase() + " " + info_users!!.firstname

        val response = requestUser!!.setUser(getToken(), Gson().toJson(info_users!!))

        if(response){

            if(numeroCarte.isEmpty() || expirationCarte.isEmpty() || codeCarte.isEmpty())
                storage.storeInitCard(false)
            else
                storage.storeInitCard(true)

            Snackbar.make(this@MonCompteFragment.view!!, "Carte de paiement enregistrée avec succès !", Snackbar.LENGTH_LONG).show()



        }


    }

    fun CallbackSaveProfil(adresse: String, ville: String, code_postal: String, telephone: String, date_anniversaire: String, genre: String) {

        info_users!!.lastname = info_users!!.lastname
        info_users!!.address = adresse
        info_users!!.city = ville
        info_users!!.postalCode = code_postal
        info_users!!.phone = telephone
        info_users!!.birthdate = date_anniversaire
        info_users!!.gender = genre

        val response = requestUser!!.setUser(getToken(), Gson().toJson(info_users!!))

        if(response){

            Snackbar.make(this@MonCompteFragment.view!!, "Profil enregistré avec succès !", Snackbar.LENGTH_LONG).show()

        }


    }

    private fun initializeDataUser() {

        val listType = object : TypeToken<User?>() {}.type

        info_users = Gson().fromJson<User>(requestInfoUser!!.getUser(getToken(),uuid), listType)


    }



}