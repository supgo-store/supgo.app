package io.supgo.app.ui.magasin;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.supgo.app.model.Store;
import io.supgo.keycloaker.R;

public class MagasinAdapter extends RecyclerView.Adapter<MagasinViewHolder>{

    public List<Store> listeMagasin;


    public  MagasinAdapter(List<Store> magasins){
        this.listeMagasin = magasins;
    }

    @NonNull
    @Override
    public MagasinViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {

        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_magasin_layout, parent, false);
        MagasinViewHolder magasinViewHolder = new MagasinViewHolder(view);

        return magasinViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MagasinViewHolder holder, int position) {

        holder.ville.setText(listeMagasin.get(position).getName());
        holder.uuid.setText(listeMagasin.get(position).getUUID());
        holder.longitude.setText(listeMagasin.get(position).getLongitude());
        holder.latitude.setText(listeMagasin.get(position).getLatitude());


    }

    @Override
    public int getItemCount() {
        return this.listeMagasin.size();
    }
}