package io.supgo.app.ui.detail_discount_categorie;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.io.ByteArrayOutputStream;

import io.supgo.keycloaker.R;

public class detail_DiscountCategorieViewHolder extends RecyclerView.ViewHolder {

    CardView cardView;
    TextView nom;
    TextView prix;
    TextView pourcentage;
    TextView description;
    ImageView image;
    TextView uuid;

    public detail_DiscountCategorieViewHolder(final View view){
        super(view);
        cardView = view.findViewById(R.id.card_defaut_liste_produit_discount_categorie);
        nom = view.findViewById(R.id.txt_nom_cardview_detail_discount_produit);
        prix = view.findViewById(R.id.txt_prix_cardview_detail_discount_produit);
        pourcentage = view.findViewById(R.id.txt_pourcentage_cardview_detail_discount_produit);
        image = view.findViewById(R.id.image_cardview_detail_discount_produit);
        description = view.findViewById(R.id.txt_description_cardview_detail_discount_produit);
        uuid = view.findViewById(R.id.txt_uuid_cardview_discount_categorie);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Bundle bundle_product = new Bundle();
                bundle_product.putString("nom_detail_produit", nom.getText().toString());
                bundle_product.putString("description_detail_produit", description.getText().toString());
                bundle_product.putString("prix_detail_produit", prix.getText().toString());
                bundle_product.putString("uuid_detail_produit", uuid.getText().toString());

                BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] bytes = stream.toByteArray();
                bundle_product.putByteArray("image_detail_produit", bytes);


                Navigation.findNavController(v).navigate(R.id.nav_detail_produit, bundle_product);

            }
        });





    }

}
