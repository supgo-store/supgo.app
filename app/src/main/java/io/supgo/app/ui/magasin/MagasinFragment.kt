package io.supgo.app.ui.magasin

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.supgo.app.RequestManager
import io.supgo.app.model.Store
import io.supgo.app.UtilRequest.getToken
import io.supgo.keycloaker.R

import java.io.IOException
import java.util.*


class MagasinFragment : Fragment() {
    var magasins: List<Store>? = null
    var recyclerView: RecyclerView? = null
    var requestStore: RequestManager? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_magasin, container, false)
        requestStore = RequestManager()

        try {
            initializeDataStore()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val txt_search = root.findViewById<TextView>(R.id.txt_search)
        txt_search.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val tmp_searchedStores = ArrayList<Store>()


                if (s.length != 0) {
                    tmp_searchedStores.clear()
                    for (searchedStore in magasins!!) {
                        if (searchedStore.name.substring(0, count).toLowerCase() == s.toString()) {
                            tmp_searchedStores.add(searchedStore)
                        }
                    }
                    val adapter = MagasinAdapter(tmp_searchedStores)
                    recyclerView!!.adapter = adapter
                } else {
                    val adapter = MagasinAdapter(magasins)
                    recyclerView!!.adapter = adapter
                }
            }

            override fun afterTextChanged(s: Editable) {}

        })

        val btn_delete_search = root.findViewById<ImageView>(R.id.btn_delete_search)
        btn_delete_search.setOnClickListener {
            txt_search.text = ""
            txt_search.clearFocus()
        }


        recyclerView = root.findViewById(R.id.recyclerview_magasin)
        val llm = LinearLayoutManager(this.context)
        recyclerView!!.setLayoutManager(llm)
        val adapter = MagasinAdapter(magasins)
        recyclerView!!.setAdapter(adapter)
        return root
    }

    @Throws(IOException::class)
    fun initializeDataStore() {
        magasins = ArrayList()
        val listType = object : TypeToken<ArrayList<Store?>?>() {}.type
        val stores = Gson().fromJson<ArrayList<Store>>(

                requestStore!!.getStore(getToken()), listType)
        magasins = stores.clone() as List<Store>
    }


}





