package io.supgo.app.ui.nosproduits;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.supgo.app.model.Category;
import io.supgo.app.model.Store;
import io.supgo.app.ui.magasin.MagasinViewHolder;
import io.supgo.keycloaker.R;

public class CategorieAdapter extends RecyclerView.Adapter<CategorieViewHolder> {

    public List<Category> listeCategorie;

    public  CategorieAdapter(List<Category> magasins){
        this.listeCategorie = magasins;
    }

    @NonNull
    @Override
    public CategorieViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_categorie_layout, parent, false);
        CategorieViewHolder categorieViewHolder = new CategorieViewHolder(view);

        return categorieViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategorieViewHolder holder, int position) {
        holder.nom.setText(listeCategorie.get(position).getName());
    }


    @Override
    public int getItemCount() {
        return this.listeCategorie.size();
    }

}
