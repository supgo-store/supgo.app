package io.supgo.app.ui.historique

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.supgo.app.RequestManager
import io.supgo.app.UtilRequest
import io.supgo.app.model.Cart
import io.supgo.app.model.Category
import io.supgo.keycloaker.R
import java.io.IOException
import java.util.*

class HistoriqueFragment : Fragment() {

    var carts: List<Cart>? = null
    var recyclerView: RecyclerView? = null
    var requestCart: RequestManager? = null
    val tmp_searchedCart = ArrayList<Cart>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_historique, container, false)

        requestCart = RequestManager()

        try {
            initializeDataCart()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        tmp_searchedCart.clear()


        //Filter
        for(searchedCart in carts!!){

            if(searchedCart.datetime != null){
                tmp_searchedCart.add(searchedCart)
            }
        }


        //Add Data
        recyclerView = root.findViewById(R.id.recyclerview_historique)
        val llm = LinearLayoutManager(this.context)
        recyclerView!!.setLayoutManager(llm)
        val adapter = HistoriqueAdapter(tmp_searchedCart)
        recyclerView!!.setAdapter(adapter)

        return root

    }

    @Throws(IOException::class)
    fun initializeDataCart() {
        carts = ArrayList()

        val listType = object : TypeToken<ArrayList<Cart?>?>() {}.type
        val json = "{ \"Key\": \"UserUuid\", \"Value\": \"${UtilRequest.getUserId()}\"}"

        val cartsList = Gson().fromJson<ArrayList<Category>>( requestCart!!.getCart(UtilRequest.getToken(), json), listType )

        carts = cartsList.clone() as List<Cart>

    }

}