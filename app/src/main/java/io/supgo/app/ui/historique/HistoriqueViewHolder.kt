package io.supgo.app.ui.historique

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.supgo.app.UtilRequest
import io.supgo.app.model.Cart
import io.supgo.app.model.Category
import io.supgo.app.model.Product
import io.supgo.app.ui.detail_categorie.detail_CategorieAdapter
import io.supgo.keycloaker.R
import io.supgo.keycloaker.helper.Helper
import org.w3c.dom.Text
import java.io.IOException
import java.util.ArrayList
import kotlin.coroutines.coroutineContext

class HistoriqueViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var cardView: CardView
    @JvmField
    var ville: TextView
    @JvmField
    var date: TextView
    @JvmField
    var numero_carte: TextView
    var prix: TextView
    @JvmField
    var uuid: TextView
    @JvmField
    var prixTotal: TextView

    

    init {


        cardView = view.findViewById(R.id.card_defaut_historique)
        ville = view.findViewById(R.id.txt_nom_ville_historique)
        date = view.findViewById(R.id.txt_date_historique)
        numero_carte = view.findViewById(R.id.txt_numero_carte_historique)
        prix = view.findViewById(R.id.txt_prix_historique)
        uuid = view.findViewById(R.id.txt_uuid_historique)
        prixTotal = view.findViewById(R.id.txt_prix_historique)
        cardView.setOnClickListener { view ->

            Snackbar.make(this@HistoriqueViewHolder.itemView, "La fonction historique des produits n'est pas encore disponible.", Snackbar.LENGTH_LONG).show()

        }




    }


}