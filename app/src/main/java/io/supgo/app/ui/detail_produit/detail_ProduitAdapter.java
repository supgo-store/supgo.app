package io.supgo.app.ui.detail_produit;

import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import io.supgo.app.model.Comment;
import io.supgo.app.model.Product;
import io.supgo.app.ui.detail_categorie.detail_CategorieViewHolder;
import io.supgo.keycloaker.R;

public class detail_ProduitAdapter extends RecyclerView.Adapter<detail_ProduitViewHolder> {

    public List<Comment> listeComments;

    public detail_ProduitAdapter(List<Comment> comments) {
        this.listeComments = comments;
    }

    @NonNull
    @Override
    public detail_ProduitViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {

        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_detail_comment_produit_layout, parent, false);
        detail_ProduitViewHolder detail_produitViewHolder = new detail_ProduitViewHolder(view);

        return detail_produitViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final detail_ProduitViewHolder holder, int position) {

        holder.nom.setText(listeComments.get(position).getChildUser().getFirstname() + " " +listeComments.get(position).getChildUser().getLastname());
        holder.note.setRating(listeComments.get(position).getNbStars());
        holder.commentaire.setText(listeComments.get(position).getText());

            String tmp_date[] = listeComments.get(position).getDatetime().split("-");
        holder.date.setText( tmp_date[2].substring(0,2) + "/" + tmp_date[1] + "/" + tmp_date[0]);


    }

    @Override
    public int getItemCount() {
        return this.listeComments.size();
    }

}
