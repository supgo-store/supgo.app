package io.supgo.app.ui.detail_categorie;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import io.supgo.app.model.Product;
import io.supgo.keycloaker.R;

public class detail_CategorieAdapter extends RecyclerView.Adapter<detail_CategorieViewHolder> {

    public List<Product> listeProduits;

    public detail_CategorieAdapter(List<Product> produits) {
        this.listeProduits = produits;
    }

    @NonNull
    @Override
    public detail_CategorieViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {

        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_detail_categorie_layout, parent, false);
        detail_CategorieViewHolder detail_categorieViewHolder = new detail_CategorieViewHolder(view);

        return detail_categorieViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final detail_CategorieViewHolder holder, int position) {


        holder.nom.setText(listeProduits.get(position).getName());
        holder.prix.setText(listeProduits.get(position).getPrice() + " €");
        holder.description.setText(listeProduits.get(position).getDescription());
        holder.uuid.setText(listeProduits.get(position).getUUID());

        byte[] imageBytes = Base64.decode(listeProduits.get(position).getImage(), Base64.DEFAULT);
        holder.image.setImageBitmap(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length));

    }

    @Override
    public int getItemCount() {
        return this.listeProduits.size();
    }

}
