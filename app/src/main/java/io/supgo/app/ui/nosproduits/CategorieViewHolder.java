package io.supgo.app.ui.nosproduits;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import io.supgo.keycloaker.R;

public class CategorieViewHolder extends RecyclerView.ViewHolder {

    TextView nom;
    CardView cardView;

    public CategorieViewHolder(final View view){
        super(view);

        cardView = view.findViewById(R.id.card_defaut_liste_categorie);
        nom = view.findViewById(R.id.txt_nom_cardview_categorie);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle_category = new Bundle();
                bundle_category.putString("nom_detail_categorie", nom.getText().toString());
                Navigation.findNavController(v).navigate(R.id.nav_detail_categorie, bundle_category);

            }
        });


    }

}
