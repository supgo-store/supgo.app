package io.supgo.app.ui.nosproduits

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.supgo.app.RequestManager
import io.supgo.app.UtilRequest.getToken
import io.supgo.app.model.Category
import io.supgo.keycloaker.R
import java.io.IOException
import java.util.*

class NosProduitsFragment : Fragment() {

    var categories: List<Category>? = null
    var recyclerView: RecyclerView? = null
    var requestCategory: RequestManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_nosproduits, container, false)
        val nom = root.findViewById<TextView>(R.id.txt_nom_promotion_cardview_category)

        requestCategory = RequestManager()

        try {
            initializeDataCategory()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val card_promotion: CardView = root.findViewById(R.id.card_promotion)
        card_promotion.setOnClickListener {

            val bundle_category = Bundle()
            bundle_category.putString("nom_detail_categorie", nom.text.toString())
            it.findNavController().navigate(R.id.nav_detail_discount_categorie, bundle_category )
        }


        recyclerView = root.findViewById(R.id.recyclerview_categorie)
        val llm = GridLayoutManager(this.context, 3)
        recyclerView!!.setLayoutManager(llm)
        val adapter = CategorieAdapter(categories)
        recyclerView!!.setAdapter(adapter)

        return root
    }

    @Throws(IOException::class)
    fun initializeDataCategory() {
        categories = ArrayList()

        val listType = object : TypeToken<ArrayList<Category?>?>() {}.type
        val categoriesList =
            Gson().fromJson<ArrayList<Category>>( requestCategory!!.getCategory(getToken()), listType )

        categories = categoriesList.clone() as List<Category>

    }

}