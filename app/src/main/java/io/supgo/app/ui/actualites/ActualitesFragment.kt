package io.supgo.app.ui.actualites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.supgo.app.model.Store
import io.supgo.keycloaker.R
import io.supgo.keycloaker.storage.IOAuth2AccessTokenStorage
import org.koin.android.ext.android.inject


class ActualitesFragment : Fragment() {

    val storage by inject<IOAuth2AccessTokenStorage>()
    var cardConnexionMagasin: CardView? = null
    var txt_connexionMagasin: TextView? = null
    var cardCompte: CardView? = null
    var cardAlertePaiement: CardView? = null
    var cardMagasin: CardView? = null
    var cardNosProduits: CardView? = null
    var cardPanier: CardView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_actualites, container, false)

        cardNosProduits =       root.findViewById(R.id.card_nosproduits)
        cardMagasin =           root.findViewById(R.id.card_magasin)
        cardCompte =            root.findViewById(R.id.card_compte)
        cardAlertePaiement =    root.findViewById(R.id.card_alert_compte)
        cardConnexionMagasin =  root.findViewById(R.id.card_connexion_magasin)
        txt_connexionMagasin =  root.findViewById(R.id.txt_connexion_magasin)
        cardPanier =            root.findViewById(R.id.card_panier)


        initializeDataStore()
        initializeDataPaiement()

        cardPanier!!.setOnClickListener { v ->
            Navigation.findNavController(v).navigate(R.id.nav_panier)
        }

        cardMagasin!!.setOnClickListener { v ->
            Navigation.findNavController(v).navigate(R.id.nav_magasin)
        }

        cardCompte!!.setOnClickListener { v ->
            Navigation.findNavController(v).navigate(R.id.nav_moncompte)
        }

        cardAlertePaiement!!.setOnClickListener { v ->
            Navigation.findNavController(v).navigate(R.id.nav_moncompte)
        }

        cardNosProduits!!.setOnClickListener { v ->
            Navigation.findNavController(v).navigate(R.id.nav_nosproduits)
        }
        return root
    }

    override fun onResume() {
        super.onResume()
        initializeDataStore()
    }


    fun initializeDataStore() {

        if(storage.getStoredStore() != ""){

            val type = object : TypeToken<Store?>() {}.type
            val info_store = Gson().fromJson<Store>(storage.getStoredStore(), type)

            cardConnexionMagasin!!.visibility = View.VISIBLE
            txt_connexionMagasin!!.text = info_store.name
            cardMagasin!!.visibility = View.GONE
            cardPanier!!.visibility = View.VISIBLE


        }else{
            cardConnexionMagasin!!.visibility = View.GONE
            txt_connexionMagasin!!.text = ""
            cardMagasin!!.visibility = View.VISIBLE
            cardPanier!!.visibility = View.GONE
        }

    }

    fun initializeDataPaiement(){

        if(storage.getStoredInitCar()){

            cardAlertePaiement!!.visibility = View.GONE
            cardCompte!!.visibility = View.GONE

        }else{
            cardAlertePaiement!!.visibility = View.VISIBLE
            cardCompte!!.visibility = View.VISIBLE
        }
    }

}