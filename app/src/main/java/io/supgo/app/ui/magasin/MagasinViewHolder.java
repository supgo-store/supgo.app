package io.supgo.app.ui.magasin;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;


import io.supgo.keycloaker.R;

public class MagasinViewHolder extends RecyclerView.ViewHolder {

    CardView cardView;
    TextView ville;
    TextView uuid;
    TextView longitude;
    TextView latitude;


    public MagasinViewHolder(final View view){
        super(view);
        cardView = view.findViewById(R.id.card_defaut_liste_magasin);
        ville = view.findViewById(R.id.txt_ville_cardview_magasin);
        uuid = view.findViewById(R.id.txt_uuid_cardview_magasin);
        longitude = view.findViewById(R.id.txt_longitude_cardview_magasin);
        latitude = view.findViewById(R.id.txt_latitude_cardview_magasin);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle_store = new Bundle();
                bundle_store.putString("ville_detail_magasin", ville.getText().toString());
                bundle_store.putDouble("latitude_detail_magasin", Double.parseDouble(latitude.getText().toString()));
                bundle_store.putDouble("longitude_detail_magasin", Double.parseDouble(longitude.getText().toString()));
                Navigation.findNavController(v).navigate(R.id.nav_detail_magasin, bundle_store);


            }
        });


    }

}
