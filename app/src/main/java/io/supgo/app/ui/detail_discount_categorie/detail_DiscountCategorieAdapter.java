package io.supgo.app.ui.detail_discount_categorie;

import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import io.supgo.app.model.Discount;
import io.supgo.keycloaker.R;

public class detail_DiscountCategorieAdapter extends RecyclerView.Adapter<detail_DiscountCategorieViewHolder> {

    public List<Discount> listeDiscount;

    public detail_DiscountCategorieAdapter(List<Discount> discount){
        this.listeDiscount = discount;
    }

    @NonNull
    @Override
    public detail_DiscountCategorieViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {

        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_detail_discount_categorie_layout, parent, false);
        detail_DiscountCategorieViewHolder detail_Discount_categorieViewHolder = new detail_DiscountCategorieViewHolder(view);

        return detail_Discount_categorieViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final detail_DiscountCategorieViewHolder holder, int position) {
        holder.nom.setText(listeDiscount.get(position).getChildProduct().getName());
        holder.prix.setText(listeDiscount.get(position).getChildProduct().getPrice() + " €");
        holder.pourcentage.setText("- " + listeDiscount.get(position).getPercentage() + "  %");
        holder.description.setText(listeDiscount.get(position).getChildProduct().getDescription());
        holder.uuid.setText(listeDiscount.get(position).getChildProduct().getUUID());

        byte[] imageBytes = Base64.decode(listeDiscount.get(position).getChildProduct().getImage(), Base64.DEFAULT);
        holder.image.setImageBitmap(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length));



    }
    @Override
    public int getItemCount() {
        return this.listeDiscount.size();
    }


}
