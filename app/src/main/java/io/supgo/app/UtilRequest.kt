package io.supgo.app

import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import io.supgo.app.model.ChildUser
import io.supgo.keycloaker.di.KeycloakToken
import io.supgo.keycloaker.helper.Helper
import io.supgo.keycloaker.helper.Principal
import io.supgo.keycloaker.storage.IOAuth2AccessTokenStorage
import org.koin.android.ext.android.inject

object UtilRequest : RxAppCompatActivity() {

    val storage by inject<IOAuth2AccessTokenStorage>()
    val ActionUuid_EnterStore = "b84ef2ca-1b2f-46a3-b157-0b174046e53f"
    val ActionUuid_LeaveStore = "03ba8419-a4bd-11ea-b668-0242ac160006"

    fun getToken(): String {

        return storage.getStoredAccessToken()?.accessToken.toString()
    }

    fun getStorage(): KeycloakToken? {
        return storage.getStoredAccessToken()
    }

    fun getUserId(): String? {

       return storage.getStoredAccessToken()?.apply {

            val principal = Helper.parseJwtToken(accessToken!!)
            return principal.userId

        }.toString()

    }




}