package io.supgo.app;

import android.os.StrictMode;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static io.supgo.keycloaker.Config.requestUrl;

public class RequestManager {

    public static final MediaType MEDIA_TYPE_MARKDOWN = MediaType.parse("application/json; charset=utf-8");
    public final OkHttpClient client = new OkHttpClient();


    public String getStore(String token) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "Store";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();


        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }

    }

    public String getStoreByUuid(String token, String uuid) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "Store/" + uuid;

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();

        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }

    }

    public String getCategory(String token) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "Category";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();


        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }

    }

    public String getDiscount(String token) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url =  requestUrl + "Discount";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();

        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }

    }

    public String getProduct(String token) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url =  requestUrl + "Product";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();

        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }

    }

    public String getComment(String token) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "Comment/";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();

        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }

    }

    public String getUser(String token, String uuid) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "User/" + uuid;

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();

        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }

    }

    public String getCart(String token, String json) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "Cart/filtered-list";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();


        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, json))
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }

    }

    public Boolean setComment(String token, String json) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "Comment/";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();


        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, json))
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            else return response.isSuccessful();


        }

    }

    public Boolean setUser(String token, String json) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "User/";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();


        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .put(RequestBody.create(MEDIA_TYPE_MARKDOWN, json))
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            else return response.isSuccessful();


        }

    }

    public Boolean setHistory(String token, String json) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "History/";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();


        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, json))
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            else return response.isSuccessful();


        }

    }

    public String postCart(String token, String json) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "Cart/";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();


        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, json))
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            else return response.body().string();


        }

    }

    public Boolean putCart(String token, String json) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // PARAMETERS
        final String url = requestUrl + "Cart/";

        // URL
        HttpUrl.Builder urlBuilder = HttpUrl
                .parse(url)
                .newBuilder();


        // REQUEST
        final Request request = new Request.Builder()
                .header("Authorization", "Bearer " + token)
                .url(urlBuilder.build().toString())
                .put(RequestBody.create(MEDIA_TYPE_MARKDOWN, json))
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            else return response.isSuccessful();


        }

    }

}


