package io.supgo.keycloaker.storage

import android.content.SharedPreferences
import androidx.core.provider.SelfDestructiveThread
import com.google.gson.Gson
import io.supgo.keycloaker.di.KeycloakToken
import io.supgo.keycloaker.helper.Helper

interface IOAuth2AccessTokenStorage {
    fun getStoredAccessToken(): KeycloakToken?
    fun storeAccessToken(token: KeycloakToken)
    fun hasAccessToken(): Boolean
    fun removeAccessToken()
    fun storeInitCard(init: Boolean)
    fun getStoredInitCar(): Boolean
    fun storeStore(store: String)
    fun getStoredStore(): String
    fun storeQR(QR: String)
    fun getStoredQR(): String
    fun storeFinalPrice(finalPrice: String)
    fun getStoredFinalPrice(): String
    fun storeUuidCart(uuid: String)
    fun getStoredUuidCart(): String
}

class SharedPreferencesOAuth2Storage(val prefs: SharedPreferences, val gson: Gson) : IOAuth2AccessTokenStorage {
    val ACCESS_TOKEN_PREFERENCES_KEY = "OAuth2AccessToken"
    val SAVE_CARDE_PREFERENCES_KEY = "SaveCard"
    val SAVE_UUID_STORE_PREFERENCES_KEY = "SaveStore"
    val SAVE_QR_PREFERENCES_KEY = "SaveQR"
    val SAVE_FINAL_PRICE_PREFERENCES_KEY = "SaveFinalPrice"
    val SAVE_UUID_CART_PREFERENCES_KEY = "SaveUuidCart"

    override fun getStoredAccessToken(): KeycloakToken? {
        val tokenStr = prefs.getString(ACCESS_TOKEN_PREFERENCES_KEY, null)
        return if (tokenStr == null) null
        else gson.fromJson(tokenStr, KeycloakToken::class.java)
    }

    override fun storeAccessToken(token: KeycloakToken) {
        prefs.edit()
            .putString(ACCESS_TOKEN_PREFERENCES_KEY, gson.toJson(token))
            .apply()
    }

    override fun storeInitCard(init: Boolean){
        prefs.edit()
            .putBoolean(SAVE_CARDE_PREFERENCES_KEY, init)
            .apply()
    }

    override fun getStoredInitCar(): Boolean {
        val init = prefs.getBoolean(SAVE_CARDE_PREFERENCES_KEY, false)
        return init
    }

    override fun storeStore(store: String){
        prefs.edit()
            .putString(SAVE_UUID_STORE_PREFERENCES_KEY, store)
            .apply()
    }

    override fun storeQR(QR: String){
        prefs.edit()
            .putString(SAVE_QR_PREFERENCES_KEY, QR)
            .apply()
    }

    override fun getStoredQR(): String {
        val qr = prefs.getString(SAVE_QR_PREFERENCES_KEY, "")
        return qr!!
    }

    override fun storeFinalPrice(finalPrice: String) {
        prefs.edit()
            .putString(SAVE_FINAL_PRICE_PREFERENCES_KEY, finalPrice)
            .apply()
    }

    override fun getStoredFinalPrice(): String {
        val finalPrice = prefs.getString(SAVE_FINAL_PRICE_PREFERENCES_KEY, "")
        return finalPrice!!
    }

    override fun storeUuidCart(uuid: String) {
        prefs.edit()
            .putString(SAVE_UUID_CART_PREFERENCES_KEY, uuid)
            .apply()
    }

    override fun getStoredUuidCart(): String {
        val uuid = prefs.getString(SAVE_UUID_CART_PREFERENCES_KEY, "")
        return uuid!!
    }


    override fun getStoredStore(): String {
        val store = prefs.getString(SAVE_UUID_STORE_PREFERENCES_KEY, "")
        return store!!
    }

    override fun hasAccessToken(): Boolean {
        return prefs.contains(ACCESS_TOKEN_PREFERENCES_KEY)
    }

    override fun removeAccessToken() {
        prefs.edit()
            .remove(ACCESS_TOKEN_PREFERENCES_KEY)
            .apply()
    }


}

