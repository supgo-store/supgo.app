package io.supgo.keycloaker.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Vibrator
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.supgo.QRReader.DecoderActivity
import io.supgo.app.RequestManager
import io.supgo.app.UtilRequest.ActionUuid_EnterStore
import io.supgo.app.UtilRequest.ActionUuid_LeaveStore
import io.supgo.app.UtilRequest.getToken
import io.supgo.app.UtilRequest.getUserId
import io.supgo.app.model.Cart
import io.supgo.app.model.History
import io.supgo.app.model.QRCode
import io.supgo.app.model.Store
import io.supgo.keycloaker.Config
import io.supgo.keycloaker.R
import io.supgo.keycloaker.di.IKeycloakRest
import io.supgo.keycloaker.helper.Helper
import io.supgo.keycloaker.helper.Helper.isRefreshTokenExpired
import io.supgo.keycloaker.storage.IOAuth2AccessTokenStorage
import io.supgo.keycloaker.token.RefreshTokenWorker.Companion.startPeriodicRefreshTokenTask
import kotlinx.android.synthetic.main.nav_header_main.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*


@Suppress("DEPRECATION")
class MainActivity : RxAppCompatActivity() {

    val api by inject<IKeycloakRest>()
    val storage by inject<IOAuth2AccessTokenStorage>()
    var requestManager: RequestManager? = null
    var myDialog: Dialog? = null

    private var mAppBarConfiguration: AppBarConfiguration? = null
    private val AUTHORIZATION_REQUEST_CODE = 1
    private val QR_REQUEST_CODE = 2


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startPeriodicRefreshTokenTask()


        myDialog = Dialog(this)
        requestManager = RequestManager()


        swiper?.apply {
            setOnRefreshListener {
                isRefreshing = true
                showData()
                isRefreshing = false
            }
        }

        // TOOLBAR
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        // END TOOLBAR

        // MENU BUILDER
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        mAppBarConfiguration = AppBarConfiguration.Builder(
            R.id.nav_actualites,
            R.id.nav_magasin,
            R.id.nav_panier,
            R.id.nav_moncompte,
            R.id.nav_nosproduits,
            R.id.nav_apropos,
            R.id.nav_historique
        )
            .setOpenableLayout(drawer)
            .build()

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)

        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration!!)
        NavigationUI.setupWithNavController(navigationView, navController)
        // END MENU BUILDER

        // VERSION
        try {
            val pInfo = applicationContext.packageManager.getPackageInfo(packageName, 0)
            navigationView.getHeaderView(0).findViewById<TextView>(R.id.app_vesion).text = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        //END VERSION

    }

    override fun onResume() {
        super.onResume()
        if (isRefreshTokenExpired(storage.getStoredAccessToken())){
            startActivityForResult(Intent(this, LoginActivity::class.java), AUTHORIZATION_REQUEST_CODE)
        }
        showData()



        if(storage.getStoredStore() != "")
            //Hide
            NavMagasinModifier(false)
        else
            //Show
            NavMagasinModifier(true)

    }

    @SuppressLint("CheckResult", "SetTextI18n")
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item!!.itemId){
            R.id.logout -> handleLogout()
            R.id.action_qr -> handleQR()
            R.id.action_nfc -> handleNFC()
        }


        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.options_list, menu)
        return true
    }

    override fun onBackPressed() {}

    @SuppressLint("CheckResult", "SetTextI18n")
    private fun showData() {
        val token = storage.getStoredAccessToken()
        token?.apply {
            val principal = Helper.parseJwtToken(accessToken!!)
            val navigationView = findViewById<NavigationView>(R.id.nav_view)
            val headerView = navigationView.getHeaderView(0)

            headerView.findViewById<TextView>(R.id.txt_nav_hearder_name_user).text = "${principal.name} ${principal.surname}"

        }
    }

    private fun handleQR(){

        startActivityForResult(Intent(this, DecoderActivity::class.java), QR_REQUEST_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == QR_REQUEST_CODE){

            if(resultCode == Activity.RESULT_OK){


                if(isJson(storage.getStoredQR())){

                    val type = object : TypeToken<QRCode?>() {}.type
                    val info_qr = Gson().fromJson<QRCode>(storage.getStoredQR(), type)
                    val store_uuid = info_qr.storeUuid
                    val action_type = info_qr.actionType

                    val resultStore = requestStore(store_uuid)

                    if(action_type == "EnterStore"){


                        val resultHistory = requestHistory(store_uuid, action_type)

                        if(resultStore != null && resultHistory != null){
                            storage.storeStore(Gson().toJson(resultStore))
                            ShowPopupSuccessConnexionMagasin()
                        }else
                            ShowPopupFailConnexionMagasin()


                    }else if (action_type == "LeaveStore"){


                        val resultHistory = requestHistory(store_uuid, action_type)

                        if(resultStore != null && resultHistory != null){

                            storage.storeStore("")
                            ShowPopupSuccesPurchase()
                        }

                    }else
                        ShowPopupFailConnexionMagasin()


                } else{
                    ShowPopupFailConnexionMagasin()
                }
            }
        }
    }


    @SuppressLint("CheckResult")
    private fun handleNFC(){

        Snackbar.make(this@MainActivity.currentFocus, "La fonction NFC n'est pas encore disponible.", Snackbar.LENGTH_LONG).show()

    }

    @SuppressLint("CheckResult")
    private fun handleLogout() {
        val refreshToken = storage.getStoredAccessToken()!!.refreshToken!!

        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        navController.navigate(R.id.nav_actualites)

        api.logout(Config.clientId, refreshToken)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                storage.removeAccessToken()
                this@MainActivity.startActivity(Intent(this@MainActivity, LoginActivity::class.java))
            }, {
                it.printStackTrace()
                Toast.makeText(this@MainActivity, "Error: ${it.message}", Toast.LENGTH_LONG).show()
            })


    }

    override fun onSupportNavigateUp(): Boolean {
        val navController =
            Navigation.findNavController(this, R.id.nav_host_fragment)
        return (NavigationUI.navigateUp(navController, mAppBarConfiguration!!)
                || super.onSupportNavigateUp())
    }

    override fun onDestroy() {
        super.onDestroy()
        storage.storeStore("")

    }

    private fun NavMagasinModifier(state: Boolean){
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.menu.findItem(R.id.nav_magasin).setVisible(state)

    }

    fun isJson(text: String): Boolean {

        try {
            JSONObject(text)
        } catch (ex: JSONException) {
            try {
                JSONArray(text)
            } catch (ex1: JSONException) {
                return false
            }
        }
        return true

    }

    fun requestStore(uuid: String): Store? {

        val listType = object : TypeToken<Store?>() {}.type
        val store = Gson().fromJson<Store>( requestManager!!.getStoreByUuid(getToken(), uuid), listType )
        return store

    }

    fun requestHistory(store_uuid: String, action: String): Boolean? {

        var actionUuid = ""

        when (action){

            "EnterStore" -> actionUuid = ActionUuid_EnterStore
            "LeaveStore" -> actionUuid = ActionUuid_LeaveStore

        }


        var sendHistory: History? = History(actionUuid, null, getUserId(), store_uuid)
        val response = requestManager!!.setHistory(getToken(), Gson().toJson(sendHistory))
        return response


    }

    fun ShowPopupSuccessConnexionMagasin() {
        val imageClose: ImageView
        val btn_continuer: Button

        val v: Vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        v.vibrate(500)

        myDialog!!.setContentView(R.layout.popup_connexionmagasin_success)

        btn_continuer = myDialog!!.findViewById(R.id.btn_continuer)
        btn_continuer.setOnClickListener { myDialog!!.dismiss() }

        imageClose = myDialog!!.findViewById(R.id.image_close)
        imageClose.setOnClickListener { myDialog!!.dismiss() }


        val type = object : TypeToken<QRCode?>() {}.type
        val info_qr = Gson().fromJson<QRCode>(storage.getStoredQR(), type)
        val store_uuid = info_qr.storeUuid
        var postCart: Cart? = Cart( null , getUserId(), store_uuid,null, null)
        val response = requestManager!!.postCart(getToken(), Gson().toJson(postCart))

        if(response != ""){
            storage.storeUuidCart(response.toString())
            myDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            myDialog!!.show()
        }

    }

    fun ShowPopupFailConnexionMagasin(){

       val imageClose: ImageView
       val btn_reassayer: Button

       val v: Vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
       v.vibrate(500)

       myDialog!!.setContentView(R.layout.popup_connexionmagasin_fail)

       btn_reassayer = myDialog!!.findViewById(R.id.btn_reassayer)
       btn_reassayer.setOnClickListener { myDialog!!.dismiss() }

       imageClose = myDialog!!.findViewById(R.id.image_close)
       imageClose.setOnClickListener { myDialog!!.dismiss() }

       myDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
       myDialog!!.show()

    }

    fun ShowPopupSuccesPurchase(){

        val imageClose: ImageView
        val btn_reassayer: Button
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)

        val v: Vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        v.vibrate(500)

        myDialog!!.setContentView(R.layout.popup_purchase_succes)

        btn_reassayer = myDialog!!.findViewById(R.id.btn_continuer)
        btn_reassayer.setOnClickListener { myDialog!!.dismiss() }

        imageClose = myDialog!!.findViewById(R.id.image_close)
        imageClose.setOnClickListener { myDialog!!.dismiss() }

        val type = object : TypeToken<QRCode?>() {}.type
        val info_qr = Gson().fromJson<QRCode>(storage.getStoredQR(), type)
        val store_uuid = info_qr.storeUuid
        var date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date())

        var putCart: Cart? = Cart(storage.getStoredUuidCart() ,getUserId(), store_uuid, date, storage.getStoredFinalPrice())
        val response = requestManager!!.putCart(getToken(), Gson().toJson(putCart))



        if(response){

            storage.storeUuidCart("")
            myDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            myDialog!!.show()

            navController.navigate(R.id.nav_actualites)
        }




    }



}




