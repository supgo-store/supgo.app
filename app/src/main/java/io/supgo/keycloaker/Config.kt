package io.supgo.keycloaker

object Config {
    const val clientId = "supgo-app"
    const val baseUrl = "http://auth.supgo-market.fr/auth/realms/SupGo/protocol/openid-connect"
    const val authenticationCodeUrl = "$baseUrl/auth"
    const val redirectUri = "supgo-app://keycloak"
    const val requestUrl = "http://api.supgo-market.fr/api/"
}