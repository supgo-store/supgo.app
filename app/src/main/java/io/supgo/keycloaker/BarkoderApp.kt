package io.supgo.keycloaker

import android.app.Application
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket

import io.supgo.keycloaker.di.apiTokenModule
import io.supgo.keycloaker.di.sharedPrefsModule
import org.koin.android.ext.android.startKoin
import java.net.URISyntaxException


class BarkoderApp : Application() {

    private var mSocket: Socket? = null

    override fun onCreate() {
        super.onCreate()

        mSocket = try {
            IO.socket("http://supgo-market.fr:8100/")
        } catch (e: URISyntaxException) {
            throw e
        }

        startKoin(this, listOf(apiTokenModule, sharedPrefsModule))
    }





    fun getSocket(): Socket? {
        return mSocket
    }


}